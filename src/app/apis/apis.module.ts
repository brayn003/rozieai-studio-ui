import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatListModule } from '@angular/material/list';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import {MatSelectModule} from '@angular/material/select';

import { SharedModule } from '../shared/shared.module';

import { ApisRoutingModule, routedComponents } from './apis.routing.module';
import { ApisAddComponent } from './apis.add.component';
import { ApisListComponent } from './apis.list.component';
import { ApisSkillLinkComponent } from './apis.skill.link.component';
import { ApisDataService } from './apis.data.service';

@NgModule({
  declarations: [
    ApisAddComponent,
    ApisListComponent,
    routedComponents
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    NgbModule,
    MatExpansionModule,
    MatListModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatCardModule,
    ApisRoutingModule,
    SharedModule
  ],
  exports: [
    ApisAddComponent,
    ApisListComponent
  ],
  providers: [
    ApisDataService
  ]
})
export class ApisModule { }
