import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatTableDataSource } from '@angular/material/table';
import * as _ from 'lodash';

import { ApisDataService } from './apis.data.service';

@Component({
  selector: 'apis-list',
  templateUrl: './apis.list.component.html'
})
export class ApisListComponent {
  public apis: Array<any> = [];
  public paramColumns = ['param_name', 'description', 'sample_value', 'default_value', 'is_optional'];
  public params = [];
  constructor(
    private apiData: ApisDataService
  ) {}

  private ngOnInit(): void {
    this.apiData.getAllApisFromServer().then((apis) => {
      this.apis = apis;
      this.params = apis.map(api => new MatTableDataSource(api.parameters));
      console.log(this.params);
    }).catch((err) => {
      console.log(err);
    })
  }
  
  
}