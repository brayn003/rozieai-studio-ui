import { Component } from '@angular/core';
import * as _ from 'lodash';

import { ApisDataService } from './apis.data.service';

@Component({
  selector: 'apis-add',
  templateUrl: './apis.add.component.html'
})
export class ApisAddComponent {
  public api: any = {};
  public httpProtocol: string = '';
  public err: string = '';
  public submitLoading: boolean = false;
  public submitSuccess: boolean = false;
  
  constructor(
    private apiData: ApisDataService
  ) {
    this.api = {
      api_endpoint: '',
      description: '',
      api_name: '',
      api_type: 'skill',
      parameters: []
    };
    this.httpProtocol = 'https://';
  }
  
  private initNewParams(): any {
    return {
      param_name: '',
      description: '',
      default_value: '',
      sample_value: '',
      optional: false,
      $editMode: true
    };
  }

  private validateApi(): boolean {
    this.err = '';
    this.api.api_endpoint == '' && (this.err += 'Api endpoint is required. ');
    this.api.description == '' && (this.err += 'Description is required. ');
    this.api.api_name == '' && (this.err += 'Api Name is required. ');
    this.api.parameters.length == 0 && (this.err += 'At least one parameter is required. ');
    for(let param of this.api.parameters) {
      if(param.param_name == '' || param.description == '' || param.default_value == '' || param.sample_value == '' || param.$editMode){
        this.err += 'Parameters are incomplete. ';
        break;
      }
    }
    return this.err.length == 0; 
  }

  private sanitizedApi(): any {
    let api: any = _.cloneDeep(this.api);
    api.api_endpoint = this.httpProtocol + api.api_endpoint;
    for(let param of api.parameters) {
      param.optional = param.optional ? '1': '0';
      delete param.$editMode;
    }
    return api;
  }

  public onClickAddNewParams(): void  {
    this.api.parameters.push(this.initNewParams());
  }

  public onClickConfirmParamRow(index): void  {
    this.api.parameters[index].$editMode = false;
  }

  public onClickEditParamRow(index): void  {
    this.api.parameters[index].$editMode = true;
  }

  public onClickResetParamRow(index): void  {
    this.api.parameters.splice(index, 1);
  }

  public onClickSubmit(): void  {
    if(this.validateApi()) {
      this.submitSuccess = false;
      this.submitLoading = true;
      this.apiData.postApiToServer(this.sanitizedApi()).then((res)=> {
        this.submitLoading = false;
        this.submitSuccess = true;
        console.log('done', res);
      }).catch((err) => {
        this.submitLoading = false;
        this.err = err.error.info;
        console.log('err', err);
      });
    }
  }
}