import { Component } from '@angular/core';
import * as _ from 'lodash';

import { ApisDataService } from './apis.data.service';
import { SkillsDataService } from '../skills/skills.data.service';
import { ConceptsDataService } from '../concepts/concepts.data.service';

@Component({
  selector: 'apis-skill-link',
  templateUrl: './apis.skill.link.component.html'
})
export class ApisSkillLinkComponent {
  public skills: Array<any> = [];
  public concepts: Array<any> = [];
  public apis: Array<any> = [];
  public attachedApis: Array<any> = [];
  public skillApiMap: any = {};
  public err: string = '';
  public submitSuccess = false;
  public submitLoading = false;

  constructor(
    private apiData: ApisDataService,
    private skillsData: SkillsDataService,
    private conceptsData: ConceptsDataService
  ) {
    this.skillApiMap = {
      skill_name: '',
      description: '',
      api_set: []
    }
  }

  private ngOnInit(): void {
    this.initSkills();
    this.initConcepts();
    this.initApis();
  }

  private initSkills() {
    this.skillsData.getAllSkillsFromServer().then((skills) => {
      this.skills = skills.data[0];
    });
  }

  private initConcepts() {
    this.conceptsData.getAllConceptsFromServer().then((concepts) => {
      this.concepts = concepts.data[0];
    });
  }

  private initApis() {
    this.apiData.getAllApisFromServer().then((apis) => {
      this.apis = apis.reduce((agg, api) => {
        agg[api.api_id] = api;
        return agg;
      }, {});
    })
  }

  private getEmptyAttachedApi() {
    return {
      api_id: false,
      parameter_concept_map: false,
      editable: true
    };
  }

  private attachNewApi() {
    this.attachedApis.push(this.getEmptyAttachedApi());
  }

  private removeAttachedApi(index){
    this.attachedApis.splice(index, 1);
  }

  public validate() {
    this.err = '';
    this.skillApiMap.skill_name === '' && (this.err += 'Skill name is required. ');
    this.skillApiMap.description === '' && (this.err += 'Skill description is required. ');
    this.attachedApis.length < 1 && (this.err += 'At least one api is required');

    for(let i = 0; i < this.attachedApis.length; i++){
      if(!this.attachedApis[i].api_id) {
        this.err += 'Define Apis. ';
        break;
      }
      const conceptParams = this.attachedApis[i].parameter_concept_map;
      for(let paramId in conceptParams) {
        if(!conceptParams[paramId].concept) {
          this.err += 'Concepts are not defined. ';
        }
      }
      if(this.attachedApis[i].editable){
        this.err += 'Apis are not confirmed. ';
        break;
      }
    }
  }
  
  private modelApiSet() {
    let apiSet = _.cloneDeep(this.attachedApis);
    for(let i = 0; i < this.attachedApis.length; i++) {
      delete apiSet[i].editable;
      var paramConcepts = Object.values(this.attachedApis[i].parameter_concept_map);
      apiSet[i].parameter_concept_map = paramConcepts.map((paramConcept) => {
        return {
          param_id: paramConcept.param.param_id,
          concept_name: paramConcept.concept
        }
      });
    }
    return apiSet;
  }

  private modelSkillApi() {
    let skillApiMap : any = _.cloneDeep(this.skillApiMap);
    skillApiMap.api_set = this.modelApiSet();
    return skillApiMap;
  }

  public onClickAttachNewApi() {
    this.attachNewApi();
  }

  public onSelectApi(apiId, index) {
    if(apiId !== '') {
      this.attachedApis[index].api_id = apiId;
      const params = this.apis[apiId].parameters;
      for(let i = 0; i < params.length; i++) {
        let param = params[i];
        if(!this.attachedApis[index].parameter_concept_map) {
          this.attachedApis[index].parameter_concept_map = {};
        }
        this.attachedApis[index].parameter_concept_map[param.param_id] = {
          param: param,
          concept: false
        };
      }
    } else {
      this.attachedApis[index] = this.getEmptyAttachedApi();
    }
  }
  
  public onClickEditApi(index) {
    this.attachedApis[index].editable = true;
  }
  
  public onClickConfirmApi(index) {
    this.attachedApis[index].editable = false;
  }

  public onClickRemoveApi(index) {
    this.attachedApis.splice(index, 1);
  }

  public onSelectConcept(concept, i, paramId) {
    this.attachedApis[i].parameter_concept_map[paramId].concept = concept;
  }

  public onClickSubmit() {
    this.submitLoading = true;
    this.submitSuccess = false;
    this.validate();
    if(this.err == '') {
      this.skillsData.postSkillApiToServer(this.modelSkillApi()).then((res) => {
        this.submitLoading = false;
        this.submitSuccess = true;
      }).catch((err) => {
        this.submitLoading = false;
        this.err = 'Something is not right!'
      });
    } else {
      this.submitLoading = false;
    }
  }
}

function remodelObjToArray(skillObj) {
  let skills = [];
  for(let key in skillObj) {
    skills.push({
      key,
      value: skillObj[key]
    });
  }
  return skills;
}