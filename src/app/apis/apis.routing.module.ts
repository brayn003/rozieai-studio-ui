import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ApisAddComponent } from './apis.add.component';
import { ApisListComponent } from './apis.list.component';
import { ApisSkillLinkComponent } from './apis.skill.link.component';

const routes: Routes = [{
  path: 'apis',
  children: [{
    path: '',
    component: ApisAddComponent,
  }, {
    path: 'add',
    component: ApisAddComponent,
  }, {
    path: 'list',
    component: ApisListComponent,
  }, {
    path: 'skill-link',
    component: ApisSkillLinkComponent
  }]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ],
  providers: []
})
export class ApisRoutingModule {}

export const routedComponents = [ ApisAddComponent, ApisListComponent, ApisSkillLinkComponent ];
