import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Constant } from '../app.constant';

@Injectable()
export class ApisDataService {

  constructor(
    private http: HttpClient
  ) {}

  public getAllApisFromServer(): Promise<any> {
    return this.http.get(Constant.serverUrl + 'apis/').toPromise();
  }

  public postApiToServer(api): Promise<any> {
    return this.http.post(Constant.serverUrl + 'apis/', api).toPromise();
  }

}