import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { KeysPipe } from './shared.keys.pipe';

@NgModule({
  declarations: [
    KeysPipe
  ],
  imports: [
    HttpClientModule,
    NgbModule,
  ],
  exports: [
    KeysPipe
  ],
  providers: [
  ]
})
export class SharedModule { }
