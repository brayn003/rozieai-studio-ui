import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Constant } from '../app.constant';

@Injectable()
export class SkillsDataService {

  constructor(
    private http: HttpClient
  ) {}

  public getAllSkillsFromServer(): Promise<any> {
    return this.http.get(Constant.serverUrl + 'skills/').toPromise();
  }

  public postSkillApiToServer(skillApiMap): Promise<any> {
    return this.http.post(Constant.serverUrl + 'skills/apis', skillApiMap).toPromise();
  }
}