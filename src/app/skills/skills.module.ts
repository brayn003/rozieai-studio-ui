import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { SkillsDataService } from './skills.data.service';

@NgModule({
  declarations: [
  ],
  imports: [
    HttpClientModule,
    NgbModule,
  ],
  exports: [
  ],
  providers: [
    SkillsDataService
  ]
})
export class SkillsModule { }
