import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';

import { ApisModule } from '../apis/apis.module';
import { CoreNavComponent } from './core.nav.component';

@NgModule({
  declarations: [
      CoreNavComponent
  ],
  imports: [
    RouterModule.forRoot([
        { path: '', redirectTo: '/apis/list', pathMatch: 'full' }
    ]),
    MatToolbarModule,
    MatButtonModule
  ],
  exports: [
      CoreNavComponent
  ],
})
export class CoreModule { }
