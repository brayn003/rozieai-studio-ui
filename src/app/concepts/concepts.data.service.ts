import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Constant } from '../app.constant';

@Injectable()
export class ConceptsDataService {

  constructor(
    private http: HttpClient
  ) {}

  public getAllConceptsFromServer(): Promise<any> {
    return this.http.get(Constant.serverUrl + 'concepts/').toPromise();
  }
}