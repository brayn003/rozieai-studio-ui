import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ConceptsDataService } from './concepts.data.service';

@NgModule({
  declarations: [
  ],
  imports: [
    HttpClientModule,
    NgbModule,
  ],
  exports: [
  ],
  providers: [
    ConceptsDataService
  ]
})
export class ConceptsModule { }
